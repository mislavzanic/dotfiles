#/bin/sh

alias vim="nvim"

alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

alias cp='cp -iv'
alias mv='mv -iv'

alias sudo='doas'

# pacman and yay
alias p="doas pacman"
alias pacsyu='sudo pacman -Syyu'                 # update only standard pkgs
alias parsua='paru -Sua --noconfirm'             # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'             # update standard pkgs and AUR pkgs (paru)
alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'  # remove orphaned packages

alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  # 'status' is protected name so using 'stat' instead
alias tag='git tag'
alias newtag='git tag -a'
alias merge='git merge'

alias yta='youtube-dl -x -f bestaudio/best'
alias tsm='transmission-remote'
alias ka='killall'
alias g='git'
alias z='zathura'
alias v="$EDITOR"
alias e="$EDITOR"
alias tf="terraform"
alias k="kubectl"

alias jctl="journalctl -p 3 -xb"

alias pmf="cd ~/.local/pmf"
alias scripts="cd ~/.local/bin/scripts"
alias zdt="cd ~/.config/zsh"
alias dwmc="cd ~/.config/dwm"
alias work="cd ~/.local/photomath"

alias app-test-us='gcloud config set project photomath-app-test && gcloud container clusters get-credentials gke-app-us-test --region us-central1'
alias app-test-eu='gcloud config set project photomath-app-test && gcloud container clusters get-credentials gke-app-eu-test --region europe-west3'
alias app-dev-eu='gcloud config set project photomath-app-dev && gcloud container clusters get-credentials gke-app-eu-dev --region europe-west3'
alias app-prod-eu='gcloud config set project photomath-app-prod && gcloud container clusters get-credentials gke-app-eu-prod --region europe-west3'
alias app-prod-us='gcloud config set project photomath-app-prod && gcloud container clusters get-credentials gke-app-us-prod --region us-central1'

alias ..='cd ..'
