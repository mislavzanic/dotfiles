{-# LANGUAGE LambdaCase #-}

module Custom.Keys where

    -- Custom
import Custom.Vars
import Custom.Prompt
import Custom.Scratchpads
import Custom.TreePrompt

    -- Base
import XMonad
import System.Exit
import qualified XMonad.StackSet as W


import XMonad.Util.NamedScratchpad

    -- Actions
import XMonad.Actions.Promote
import XMonad.Actions.Prefix (PrefixArgument (Raw), withPrefixArgument)
import XMonad.Actions.Submap (submap)
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.CycleWS

import XMonad.Util.ActionCycle (cycleAction)
import XMonad.Layout.SubLayouts ( GroupMsg( UnMerge
                                          , MergeAll
                                          , UnMergeAll
                                          )
                                , onGroup
                                , mergeDir
                                , pushGroup
                                )


    -- Data
import qualified Data.Map        as M

    -- Hooks
import XMonad.Hooks.ManageDocks (ToggleStruts(..))

    -- Prompt
import XMonad.Prompt
import XMonad.Prompt.Shell (shellPrompt)
import XMonad.Prompt.Pass
import XMonad.Prompt.Window

type Keybinding = (String, X ())

switchToLayout :: String -> X ()
switchToLayout = sendMessage . JumpToLayout

windowsKeys =
  [ ("M-v g", windowPrompt dtXPConfig Goto allWindows)
  , ("M-v b", windowPrompt dtXPConfig Bring allWindows)

  , ("M-j", windows W.focusDown)
  , ("M-k", windows W.focusUp)
  , ("M-m", windows W.focusMaster)

  , ("M-<Backspace>", promote)

  , ("M-S-j", windows W.swapDown)
  , ("M-S-k", windows W.swapUp)

  , ("M-h", sendMessage Shrink)
  , ("M-l", sendMessage Expand)
  ]

workspaceKeys =
  [("M-" ++ m ++ k, windows $ f i)
  | (i, k) <- zip myWorkspaces (map show [1 :: Int ..])
  , (f, m) <- [(W.greedyView, ""), (W.shift, "S-")]] ++

  [ ("M-M1-<Tab>", moveTo Next NonEmptyWS)

  , ("M-M1-S-<Tab>", moveTo Prev NonEmptyWS)

  , ("M-n r", renameWorkspace dtXPConfig)

  , ("M-n a", addWorkspacePrompt dtXPConfig)

  , ("M-S-<Backspace>", removeWorkspace)

  , ("M-n m", withWorkspace dtXPConfig (windows . W.shift))

  , ("M-n n", selectWorkspace dtXPConfig)
  ]

passPromptKeys =
  [("M-x p", passPrompt dtXPConfig)
  ,("M-x a", passGeneratePrompt dtXPConfig)
  ,("M-x r", passRemovePrompt dtXPConfig)
  ]

promptKeys =
  [ ("M-p", shellPrompt dtXPConfig)
  , ("M-o l", systemPrompt dtXPConfig)
  , ("M-s", withPrefixArgument $ submap . searchEngineMap . \case
        Raw _ -> Selected  -- Search for selected text.
        _     -> Normal)   -- Search for a certain term.
  ]

layoutKeys =
  [ ("M-<Space>", sendMessage ToggleStruts)
  , ("M-<Tab>", sendMessage NextLayout)

  , ("M-S-l t", switchToLayout "Tall")
  , ("M-S-l h", switchToLayout "Hacking")
  , ("M-S-l f", switchToLayout "Full")
  , ("M-C-m"  , cycleAction "tabAll" [ withFocused $ sendMessage . MergeAll
                                     , withFocused $ sendMessage . UnMergeAll
                                     ]
    )
  , ("M-;"  , withFocused $ sendMessage . mergeDir id)
  , ("M-S-;"  , withFocused (sendMessage . UnMerge) *> windows W.focusUp)
  ]

appKeys =
  [("M-<Return>", spawn myTerminal)

  , ("M-S-c", kill)

  , ("M-]", spawn "pamixer -i 5")
  , ("M-[", spawn "pamixer -d 5")

  , ("M-S-y", spawn "youtube.sh")

  , ("M-b", spawn myBrowser)
  , ("M-z", spawn myPDF)
  , ("M-o q", spawn "dmscrot")
  , ("M-o k", spawn "dmkill")
  , ("M-o b", spawn "set_bg")

  , ("M-u", spawn myEditor)

  -- Quit xmonad
  , ("M-S-q", io exitSuccess)
  , ("M-q", spawn "xmonad --recompile; xmonad --restart")
  ]

scratchPadKeys =
  [ ("M-C-<Return>", namedScratchpadAction myScratchPads "terminal")
  , ("M-C-s", namedScratchpadAction myScratchPads "spotify")
  ]

myKeys = concat
  [ appKeys
  , scratchPadKeys
  , layoutKeys
  , promptKeys
  , windowsKeys
  , workspaceKeys
  , passPromptKeys
  ]

------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
------------------------------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]
