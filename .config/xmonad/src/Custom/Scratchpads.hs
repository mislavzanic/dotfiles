module Custom.Scratchpads where

import XMonad
import Custom.Vars
import XMonad.Util.NamedScratchpad
import XMonad.StackSet

myScratchPads :: [NamedScratchpad]
myScratchPads = [ NS "terminal" spawnTerm findTerm manageTerm
                , NS "spotify" spawnSpotify findSpotify manageSpotify
                ]
  where
    spawnTerm  = myTerminal ++ " -t scratchpad"
    findTerm   = title =? "scratchpad"
    manageTerm = customFloating $ RationalRect l t w h
                 where
                   h = 0.9
                   w = 0.9
                   t = 0.95 - h
                   l = 0.95 - w
    spawnSpotify  = myTerminal ++ " -t spotify -e spotify"
    findSpotify   = title =? "Spotify" <||> title =? "spotify" <||> className =? "Spotify"
    manageSpotify = customFloating $ RationalRect l t w h
                    where
                      h = 0.9
                      w = 0.9
                      t = 0.95 - h
                      l = 0.95 - w
